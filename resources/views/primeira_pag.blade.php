
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css" type="text/css">
</head>
<body>
    <h1>Recuperação 2 - Laravel</h1>
    <hr>
    <h4>ALUNO: Lucas Gomes Neto - TURMA: Aut2D1</h4>
    <hr>
    <h4>Cadastro de Usuários</h4>
    <form action="/salvar" method="GET">
        <div><p>Usuário: <input type="name" name="name" ></p></div>
        <div><p>Email: <input type="email" name="email"></p></div>
        <div><p>Senha: <input type="password" name="password"></p></div>
        <input type="submit" name="submit" value="Entrar" class="btn btn-primary">
    </form>
</body>
</html>