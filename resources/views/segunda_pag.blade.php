<?php 
  $name = $_GET["name"];
  $email = $_GET["email"];
  $password = $_GET["password"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css" type="text/css">
</head>
<body>
    <h1>Recuperação 2 - Laravel</h1>
    <hr>
    <h4>ALUNO: Lucas Gomes Neto - TURMA: Aut2D1</h4>
    <hr>
    <h4>Dados enviados para o banco de dados</h4>
    <p>Usuário: <?php echo $name?></p>
    <p>Email: <?php echo $email?></p>
    <p>Senha: <?php echo md5($password)?></p>
    <form action="/" method="GET">
      <input type="submit" value="Voltar" class="btn btn-primary" >
    </form>
</body>
</html>